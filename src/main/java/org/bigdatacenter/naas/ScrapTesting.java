package org.bigdatacenter.naas;

import org.jsoup.Jsoup;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

/**
 * Created by HP1 on 2017-01-20.
 */
public class ScrapTesting {

    public static void main(String args[]) throws Exception{
    	
//    	getMajorResearchAchievement();
//    	getTestResearchReport();
    	
//    	getTechnologyTransfer();
    	getPaperPublication();
    }
    
    
    
    
    public static void print(String string) {
		System.out.println(string);
	}
    
    
    //주요연구성과
    public static void getMajorResearchAchievement() {
    	print("running...");
		/*
		 * Map<String,String> pageCookie = new HashMap<String, String>();
		 * pageCookie.put("JSESSIONID",
		 * "YDtu5LDI5xxH3iBl4aQdQtlSswIzgoMmBye1S7aCUqFhxEg3UX4eiEtFPwFKHV7z.NAAS-AP2-mw4_servlet_engine1"
		 * ); pageCookie.put("naasColor", "1"); pageCookie.put("WT_FPC",
		 * "id=2185b4e05068efc1b6c1559634410585"); pageCookie.put("WT_FPC",
		 * "id=2185b4e05068efc1b6c1559634410585"); pageCookie.put("lv",
		 * "1559636715885"); pageCookie.put("ss", "1559634410585");
		 */
		
    	// Set initialized Data because the page make a post request to get data
		Map<String, String> initialData = new HashMap<String, String>();
		initialData.put("tg","0");
		initialData.put("mmode","11");
		initialData.put("menu_code","2");
		initialData.put("viewMode","list");
		
		String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36";
		
		try {
			
			//Get Document object after parsing the html from given url.
		
			
			Document documentTotalCount = Jsoup.connect("http://www.naas.go.kr/02_research/Research_List.do")
			           .userAgent(userAgent)
                       .timeout(3000)
                       .header("Origin", "http://www.naas.go.kr")
                       .header("Referer", "http://www.naas.go.kr/")
                       .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
                       .header("Content-Type", "application/x-www-form-urlencoded")
                       .header("Accept-Encoding", "gzip, deflate")
                       .header("Accept-Language", "en-US,en;q=0.9,km;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5,zh-TW;q=0.4,la;q=0.3,ja;q=0.2,ko;q=0.1")
//                       .cookies(pageCookie)
                       .data(initialData)
                       .post();

			Element elementTotalCount = documentTotalCount.select(".bbsTotal").first();
			String[] split = elementTotalCount.text().replaceAll("총글수 :", "").replaceAll("건", "").replaceAll(",", "").split(" ");
			//Split to get total count
			Long totalCount = Long.valueOf(split[1]);
			// get total page
	        Long totalPage = (long) Math.ceil((double)totalCount);
	        print("totalPage "+totalPage);
	        for(int page=1; page<=totalCount; page++) {
	        	// make a new request for all pages
	        	Map<String, String> data = new HashMap<String, String>();
				data.put("tg","0");
				data.put("mmode","11");
				data.put("mmodeNM", "주요연구성과");
				data.put("menu_code","2");
				data.put("page",String.valueOf(page));
				
	        	Document document =  Jsoup.connect("http://www.naas.go.kr/02_research/Research_List.do")
				           .userAgent(userAgent)
	                       .timeout(3000)
	                       .header("Origin", "http://www.naas.go.kr")
	                       .header("Referer", "http://www.naas.go.kr/")
	                       .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
	                       .header("Content-Type", "application/x-www-form-urlencoded")
	                       .header("Accept-Encoding", "gzip, deflate")
	                       .header("Accept-Language", "en-US,en;q=0.9,km;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5,zh-TW;q=0.4,la;q=0.3,ja;q=0.2,ko;q=0.1")
//	                       .cookies(pageCookie )
	                       .data(data)
	                       .post();
	        	Elements elements = document.select("table.bbsList >tbody >tr");
	        	
	        	for(Element element : elements) {
	        		
	        		if(element.text().contains("검색결과가 없습니다.")) {
	        			break;
	        		}else {
	        			
	     
	        			print(element.text());
	        			
	        			
	        		}
	        	}
	        }
	           
		} catch (IOException e) {
			e.printStackTrace();
		}
    		print("done");
    }
    
    
    //시험연구보고서
    public static void getTestResearchReport() {
    	print("running...");
		
    	// Set initialized Data because the page make a post request to get data
		Map<String, String> initialData = new HashMap<String, String>();
		initialData.put("tg","1");
		initialData.put("mmode","21");
		initialData.put("menu_code","2");
		initialData.put("viewMode","list");
		
		String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36";
		
		

		try {
			
			//Get Document object after parsing the html from given url.
		
			
			Document documentTotalCount = Jsoup.connect("http://www.naas.go.kr/02_research/Research_List3.do")
			           .userAgent(userAgent)
                       .timeout(3000)
                       .header("Origin", "http://www.naas.go.kr")
                       .header("Referer", "http://www.naas.go.kr/")
                       .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
                       .header("Content-Type", "application/x-www-form-urlencoded")
                       .header("Accept-Encoding", "gzip, deflate")
                       .header("Accept-Language", "en-US,en;q=0.9,km;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5,zh-TW;q=0.4,la;q=0.3,ja;q=0.2,ko;q=0.1")
//                       .cookies(pageCookie)
                       .data(initialData)
                       .post();

			Element elementTotalCount = documentTotalCount.select(".bbsTotal").first();
			String[] split = elementTotalCount.text().replaceAll("총글수 :", "").replaceAll("건", "").replaceAll(",", "").split(" ");
			//Split to get total count
			Long totalCount = Long.valueOf(split[1]);
			// get total page
	        Long totalPage = (long) Math.ceil((double)totalCount);
	        print("totalRecord "+totalCount);
	        
	        // get year from comboboxes
	        Elements yearComboboxes = documentTotalCount.select("#searchYear");
	        String yearComboBoxesValue = yearComboboxes.text().replaceAll("연도", "");
	        String[] year = yearComboBoxesValue.split("년");
	        
	        for(int i =0; i<year.length; i++) {
	        	print(year[i]);
	        	Map<String, String> data = new HashMap<String, String>();
				data.put("tg","1");
				data.put("mmode","21");
				data.put("mmodeNm", "시험연구보고서");
				data.put("menu_code","2");
				data.put("searchYear", year[i]);
	        	
	        	for(int page=1; page<=totalCount; page++) {
		        	// make a new request for all pages
	        		data.put("page",String.valueOf(page));
					
		        	Document document =  Jsoup.connect("http://www.naas.go.kr/02_research/Research_List3.do")
					           .userAgent(userAgent)
		                       .timeout(3000)
		                       .header("Origin", "http://www.naas.go.kr")
		                       .header("Referer", "http://www.naas.go.kr/")
		                       .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
		                       .header("Content-Type", "application/x-www-form-urlencoded")
		                       .header("Accept-Encoding", "gzip, deflate")
		                       .header("Accept-Language", "en-US,en;q=0.9,km;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5,zh-TW;q=0.4,la;q=0.3,ja;q=0.2,ko;q=0.1")
//		                       .cookies(pageCookie)
		                       .data(data)
		                       .post();
		        	Elements elements = document.select("table.bbsList >tbody >tr");
			        
		        	for(Element element : elements) {
		        	
		        		if(element.text().contains("검색결과가 없습니다.")) {
		        			break;
		        		}else {
		        			print(element.text());
		        		}
		        		
		        	}
		        }
	 
	        }
	
	        
	           
		} catch (IOException e) {
			e.printStackTrace();
		}
    		print("done");
    }

    
    //기술이전
    public static void getTechnologyTransfer() {
    	print("running...");
		
    	// Set initialized Data because the page make a post request to get data
		Map<String, String> initialData = new HashMap<String, String>();
		initialData.put("tg","5");
		initialData.put("mmode","34");
		initialData.put("menu_code","2");
		initialData.put("viewMode","list");
		
		String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36";
		
		

		try {
			
			//Get Document object after parsing the html from given url.
		
			
			Document documentTotalCount = Jsoup.connect("http://www.naas.go.kr/02_research/Research_List3.do")
			           .userAgent(userAgent)
                       .timeout(3000)
                       .header("Origin", "http://www.naas.go.kr")
                       .header("Referer", "http://www.naas.go.kr/")
                       .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
                       .header("Content-Type", "application/x-www-form-urlencoded")
                       .header("Accept-Encoding", "gzip, deflate")
                       .header("Accept-Language", "en-US,en;q=0.9,km;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5,zh-TW;q=0.4,la;q=0.3,ja;q=0.2,ko;q=0.1")
//                       .cookies(pageCookie)
                       .data(initialData)
                       .post();

			Element elementTotalCount = documentTotalCount.select(".bbsTotal").first();
			String[] split = elementTotalCount.text().replaceAll("총글수 :", "").replaceAll("건", "").replaceAll(",", "").split(" ");
			//Split to get total count
			Long totalCount = Long.valueOf(split[1]);
			// get total page
	        Long totalPage = (long) Math.ceil((double)totalCount);
	        print("totalRecord "+totalCount);
	        
	        // get year from comboboxes
	        Elements yearComboboxes = documentTotalCount.select("#searchYear");
	        String yearComboBoxesValue = yearComboboxes.text().replaceAll("연도", "");
	        String[] year = yearComboBoxesValue.split("년");
	        
	        for(int i =0; i<year.length; i++) {
	        	print(year[i]);
	        	Map<String, String> data = new HashMap<String, String>();
				data.put("tg","5");
				data.put("mmode","34");
				data.put("mmodeNm", "기술이전");
				data.put("menu_code","2");
				data.put("searchYear", year[i].trim());
				
	        	
	        	for(int page=1; page<=totalCount; page++) {
		        	// make a new request for all pages
	        		data.put("page",String.valueOf(page));
					
		        	Document document =  Jsoup.connect("http://www.naas.go.kr/02_research/Research_List3.do")
					           .userAgent(userAgent)
		                       .timeout(3000)
		                       .header("Origin", "http://www.naas.go.kr")
		                       .header("Referer", "http://www.naas.go.kr/")
		                       .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
		                       .header("Content-Type", "application/x-www-form-urlencoded")
		                       .header("Accept-Encoding", "gzip, deflate")
		                       .header("Accept-Language", "en-US,en;q=0.9,km;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5,zh-TW;q=0.4,la;q=0.3,ja;q=0.2,ko;q=0.1")
//		                       .cookies(pageCookie)
		                       .data(data)
		                       .post();
		        	Elements elements = document.select("table.bbsList >tbody >tr");
			        
		        	for(Element element : elements) {
		        	
		        		if(element.text().contains("데이터가 없습니다.")) {
		        			break;
		        		}else {
		        			print(element.text());
		        		}
		        		
		        	}
		        }
	 
	        }
	
	        
	           
		} catch (IOException e) {
			e.printStackTrace();
		}
    		print("done");
    }
   
    
    //논문게재
    public static void getPaperPublication() {
    	
    	String[] years = getFilterYear();
    	
    	for(int i=0; i<years.length;i++) {
    		print("running...");
        	print("year"+years[i]);
        	// Set initialized Data because the page make a post request to get data
    		Map<String, String> initialData = new HashMap<String, String>();
    		initialData.put("tg","6");
    		initialData.put("mmode","36");
    		initialData.put("menu_code","2");
    		initialData.put("viewMode","list");
    		initialData.put("searchYear",years[i].trim()); 
    		
    		String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36";

    		try {
    			
    			//Get Document object after parsing the html from given url.
    		
    		
    		System.out.println(years[i]);
    		Document documentTotalCount = Jsoup.connect("http://www.naas.go.kr/02_research/Research_List3.do")
    			           .userAgent(userAgent)
                           .timeout(100000)
                           .header("Origin", "http://www.naas.go.kr")
                           .header("Referer", "http://www.naas.go.kr/02_research/Research_List3.do")
                           .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
                           .header("Content-Type", "application/x-www-form-urlencoded")
                           .header("Accept-Encoding", "gzip, deflate")
                           .header("Accept-Language", "en-US,en;q=0.9,km;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5,zh-TW;q=0.4,la;q=0.3,ja;q=0.2,ko;q=0.1")
//                           .cookies(pageCookie)
                           .data(initialData)
                           .post();

    			Element elementTotalCount = documentTotalCount.select(".bbsTotal").first();
    			String[] split = elementTotalCount.text().replaceAll("총글수 :", "").replaceAll("건", "").replaceAll(",", "").split(" ");
    			//Split to get total count
    			Long totalCount = Long.valueOf(split[1]);
    			// get total page
    	        Long totalPage = (long) Math.ceil((double)totalCount);
    	        print("totalRecord "+totalCount);
    	        
    	        Elements elements = documentTotalCount.select("table.bbsList > tbody > tr");
    	        for(Element e: elements) {
    	        	print(e.text());
    	        }
    	       	
    	       
    	           
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	
    		print("done");
    }
    
    // get year
    
    public static String[] getFilterYear() {
    	print("running...");
    	
    	// Set initialized Data because the page make a post request to get data
		Map<String, String> initialData = new HashMap<String, String>();
		initialData.put("tg","6");
		initialData.put("mmode","36");
		initialData.put("menu_code","2");
		initialData.put("viewMode","list");
		initialData.put("searchYear", "2014");
		
		String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36";
		
		

		try {
			
			//Get Document object after parsing the html from given url.
		
			
			Document documentTotalCount = Jsoup.connect("http://www.naas.go.kr/02_research/Research_List3.do")
			           .userAgent(userAgent)
                       .timeout(3000)
                       .header("Origin", "http://www.naas.go.kr")
                       .header("Referer", "http://www.naas.go.kr/")
                       .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
                       .header("Content-Type", "application/x-www-form-urlencoded")
                       .header("Accept-Encoding", "gzip, deflate")
                       .header("Accept-Language", "en-US,en;q=0.9,km;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5,zh-TW;q=0.4,la;q=0.3,ja;q=0.2,ko;q=0.1")
//                       .cookies(pageCookie)
                       .data(initialData)
                       .post();

	     
	        // get year from comboboxes
	        Elements yearComboboxes = documentTotalCount.select("#searchYear");
	        String yearComboBoxesValue = yearComboboxes.text().replaceAll("연도", "");
	        String[] year = yearComboBoxesValue.split("년");
	        
	        
	       return year;
	 
	       
	        
	           
		} catch (IOException e) {
			e.printStackTrace();
		}
    		print("done");
			return null;
    }
}
