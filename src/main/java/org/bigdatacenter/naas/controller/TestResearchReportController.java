package org.bigdatacenter.naas.controller;

import org.bigdatacenter.naas.service.TestResearchReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/scrapTestResearchReport")
public class TestResearchReportController {
	@Autowired
	private TestResearchReportService testResearchReportService;

	
	@RequestMapping(method = RequestMethod.GET)
    public String scrapTestResearchReport() {
		if(testResearchReportService.scrapTestResearchReport()) {
			return "SUCCESS";
		}else {
			return "FAILED";
		}
		
	}
	
	@RequestMapping(method = RequestMethod.GET,value="/noFilter")
	public String scrapTestResearchReportNoFilter() {
		if(testResearchReportService.scrapTestResearchReportNoFilter()) {
			return "SUCCESS";
		}else {
			return "FAILED";
		}
	}
}
