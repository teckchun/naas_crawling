package org.bigdatacenter.naas.service;

import org.springframework.stereotype.Service;

public interface PublicationService {

	public boolean scrapPublication();
	
	public boolean testInsert();
}
