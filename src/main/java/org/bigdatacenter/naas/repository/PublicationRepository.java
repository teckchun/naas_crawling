package org.bigdatacenter.naas.repository;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.bigdatacenter.naas.model.Publication;
import org.springframework.stereotype.Repository;

@Repository
public interface PublicationRepository {
	
	@Insert("<script> "+
			"INSERT INTO NAAS_DB.논문게재(번호,연도,분야,제목,주저자,구분)"
			+ "VALUES"
			+ "<foreach collection='publications' item='publication' separator=',' >" +
			" (#{publication.NO},"
			+ "#{publication.publishedYear},"
			+ "#{publication.field},"
			+ "#{publication.title},"
			+ "#{publication.mainAuthor},"
			+ "#{publication.division})"
			+ "</foreach> " 
			+ "</script> ")
    public void saveBatch(@Param("publications") List<Publication> publications);
	
	
	@Insert("INSERT INTO NAAS_DB.논문게재(번호,연도,분야,제목,주저자,구분)" 
			+"VALUES(#{NO},"
			+ "#{publishedYear},"
			+ "#{field},"
			+ "#{title},"
			+ "#{mainAuthor},"
			+ "#{division})")
	public void save(Publication publication);
	

}
