package org.bigdatacenter.naas.service;

public interface TestResearchReportService {
	public boolean scrapTestResearchReport();
	public boolean scrapTestResearchReportNoFilter();
}
