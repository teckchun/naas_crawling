package org.bigdatacenter.naas.controller;

import org.bigdatacenter.naas.service.TechnologyTransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/scrapTechnologyTransfer")
public class TechnologyTransferController {
	@Autowired
	private TechnologyTransferService technologyTransferService;

	
	@RequestMapping(method = RequestMethod.GET)
    public String scrapTechnologyTransfer() {
		if(technologyTransferService.scrapTechnologyTransfer()) {
			return "SUCCESS";
		}else {
			return "FAILED";
		}
		
	}
	
	@RequestMapping(method = RequestMethod.GET,value="/noFilter")
    public String scrapTechnologyTransferNoFilter() {
		if(technologyTransferService.scrapTechnologyTransferNoFilter()) {
			return "SUCCESS";
		}else {
			return "FAILED";
		}
		
	}
}
