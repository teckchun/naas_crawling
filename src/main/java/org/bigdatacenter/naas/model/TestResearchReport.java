package org.bigdatacenter.naas.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TestResearchReport extends BaseModel {
	
	
	@JsonProperty("연구자")
	private String researcher;

	public String getResearcher() {
		return researcher;
	}

	public void setResearcher(String researcher) {
		this.researcher = researcher;
	}
	
	
}
