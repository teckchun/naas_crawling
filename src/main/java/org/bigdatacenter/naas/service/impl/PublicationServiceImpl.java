package org.bigdatacenter.naas.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bigdatacenter.naas.model.Publication;
import org.bigdatacenter.naas.repository.PublicationRepository;
import org.bigdatacenter.naas.service.PublicationService;
import org.bigdatacenter.naas.util.PrintLog;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PublicationServiceImpl implements PublicationService {
	
	final static String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36";
	final static String ORIGIN = "http://www.naas.go.kr";
	final static String REFERER = "http://www.naas.go.kr/";
	final static String ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3";
	final static String CONTENT_TYPE = "application/x-www-form-urlencoded";
	final static String ACCEPT_ENCODING = "gzip, deflate";
	final static String ACCEPT_LANGUAGE = "en-US,en;q=0.9,km;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5,zh-TW;q=0.4,la;q=0.3,ja;q=0.2,ko;q=0.1";
	final static String URL = "http://www.naas.go.kr/02_research/Research_List3.do";

	@Autowired
	PublicationRepository publicationRepository;
	
	@Override
	public boolean scrapPublication() {
		// TODO Auto-generated method stub
		String[] years = getFilterYear();
		List<Publication> publications = new ArrayList<>();
    	
    	for(int i=0; i<years.length;i++) {
    		
    		PrintLog.print("running...");
    		PrintLog.print("year"+years[i]);
        	// Set initialized Data because the page make a post request to get data
    		Map<String, String> initialData = new HashMap<String, String>();
    		// Check request headers
    		initialData.put("tg","6");
    		initialData.put("mmode","36");
    		initialData.put("menu_code","2");
    		initialData.put("viewMode","list");
    		initialData.put("searchYear", years[i].trim());
    		try {
    			Document documentTotalCount = Jsoup.connect(URL)
    			           .userAgent(USER_AGENT)
                           .timeout(100000)
                           .header("Origin", ORIGIN)
                           .header("Accept", ACCEPT)
                           .header("Content-Type", CONTENT_TYPE)
                           .header("Accept-Encoding", ACCEPT_ENCODING)
                           .header("Accept-Language", ACCEPT_LANGUAGE)
                           .data(initialData)
                           .post();

    			Element elementTotalCount = documentTotalCount.select(".bbsTotal").first();
    			String[] split = elementTotalCount.text().replaceAll("총글수 :", "").replaceAll("건", "").replaceAll(",", "").split(" ");
    			//Split to get total count
       			Long totalCount = Long.valueOf(split[1]);
    	        PrintLog.print("totalRecord "+totalCount);
    	        for(int page=1; page<=totalCount; page++) {
    	        	Map<String, String> data = new HashMap<String, String>();
    				data.put("tg","6");
    				data.put("mmode","36");
    				data.put("mmodeNM", "논문게재");
    				data.put("menu_code","2");
    				data.put("page",String.valueOf(page));
    				data.put("searchYear", years[i].trim());
    				// MAKE REQUEST TO GET DATA OF EACH YEAR AND EACH PAGE
    	        	Document document =  Jsoup.connect("http://www.naas.go.kr/02_research/Research_List3.do")
    			           .userAgent(USER_AGENT)
                           .timeout(100000)
                           .header("Origin", ORIGIN)
                           .header("Accept", ACCEPT)
                           .header("Content-Type", CONTENT_TYPE)
                           .header("Accept-Encoding", ACCEPT_ENCODING)
                           .header("Accept-Language", ACCEPT_LANGUAGE)
                           .data(data)
                           .post();
    	        	
    	        	Elements elements = document.select("table.bbsList >tbody >tr:not(:first-child)");
    	    
    	        	for(Element element : elements) {
    	        		if(element.text().contains("데이터가 없습니다.")) {
    	        			break;
    	        		}else {
    	        			PrintLog.print(element.text());
    	        			Elements elePublications = element.select("td");
    	        			Publication publication = new Publication();
    	        			
    	        			
    	        			publication.setNO(elePublications.get(0).text());
    	        			publication.setPublishedYear(elePublications.get(1).text());
    	        			publication.setField(elePublications.get(2).text());
    	        			publication.setTitle(elePublications.get(3).text());
    	        			publication.setMainAuthor(elePublications.get(4).text());
    	        			publication.setDivision(elePublications.get(5).text());
    	        			
    	        			publications.add(publication);
    	        			
    	        			
    	        			
    	        			
    	        			
    	        			
    	        		}
    	        	}
    	        }
    	        
    	        PrintLog.print("size: "+publications.size());
    	        
			
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	publicationRepository.saveBatch(publications);
    	
    	PrintLog.print("done");
		return true;
	}

	
	public static String[] getFilterYear() {
		PrintLog.print("running...");
    	
    	// Set initialized Data because the page make a post request to get data
		Map<String, String> initialData = new HashMap<String, String>();
		initialData.put("tg","6");
		initialData.put("mmode","36");
		initialData.put("menu_code","2");
		initialData.put("viewMode","list");
		
		try {
			Document documentTotalCount = Jsoup.connect("http://www.naas.go.kr/02_research/Research_List3.do")
			           .userAgent(USER_AGENT)
                       .timeout(3000)
                       .header("Origin", ORIGIN)
                       .header("Referer", REFERER)
                       .header("Accept", ACCEPT)
                       .header("Content-Type", CONTENT_TYPE)
                       .header("Accept-Encoding", ACCEPT_ENCODING)
                       .header("Accept-Language", ACCEPT_LANGUAGE)
                       .data(initialData)
                       .post();

	     
	        // get year from comboboxes
	        Elements yearComboboxes = documentTotalCount.select("#searchYear");
	        String yearComboBoxesValue = yearComboboxes.text().replaceAll("연도", "");
	        String[] year = yearComboBoxesValue.split("년");
	        
	        
	       return year;
	 
	       
	        
	           
		} catch (IOException e) {
			e.printStackTrace();
		}
    		PrintLog.print("done");
			return null;
    }
	
	@Override
	public boolean testInsert() {
		// TODO Auto-generated method stub
		Publication publication = new Publication();
		publication.setNO("1");
		publication.setPublishedYear("2019");
		publication.setField("computer science");
		publication.setTitle("C++");
		publication.setDivision("SCI");
		
		publicationRepository.save(publication);
		
		return true;
	}
}
