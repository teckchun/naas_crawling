package org.bigdatacenter.naas.repository;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.bigdatacenter.naas.model.Publication;
import org.bigdatacenter.naas.model.ResearchArchievement;
import org.springframework.stereotype.Repository;

@Repository
public interface ResearchArchievementRepository {
	@Insert("<script> "+
			"INSERT INTO NAAS_DB.주요연구성과(번호,연도,분야,제목,작성자,상세내역)"
			+ "VALUES"
			+ "<foreach collection='researchArchievements' item='researchArchievement' separator=',' >" +
			" (#{researchArchievement.NO},"
			+ "#{researchArchievement.publishedYear},"
			+ "#{researchArchievement.field},"
			+ "#{researchArchievement.title},"
			+ "#{researchArchievement.writer},"
			+ "#{researchArchievement.detail})"
			+ "</foreach> " 
			+ "</script> ")
    public void saveBatch(@Param("researchArchievements") List<ResearchArchievement> researchArchievements);
}
