package org.bigdatacenter.naas.controller;

import java.util.List;
import org.bigdatacenter.naas.model.Publication;
import org.bigdatacenter.naas.service.PublicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.annotations.ApiIgnore;

@Controller
@RequestMapping("/scrapPublication")
public class PublicationController {
	
	@Autowired
	private PublicationService publicationService;

	
	@RequestMapping(method = RequestMethod.GET)
    public String scrapPublication() {
		if(publicationService.scrapPublication()) {
			return "SUCCESS";
		}else {
			return "FAILED";
		}
		
	}
	
}
