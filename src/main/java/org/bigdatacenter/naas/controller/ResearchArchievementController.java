package org.bigdatacenter.naas.controller;

import org.bigdatacenter.naas.service.ResearchArchievementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/scrapResearchArchievement")
public class ResearchArchievementController {
	@Autowired
	private ResearchArchievementService researchArchievementService;

	
	@RequestMapping(method = RequestMethod.GET)
    public String scrapPublication() {
		if(researchArchievementService.scrapResearchArchievement()) {
			return "SUCCESS";
		}else {
			return "FAILED";
		}
		
	}
}
