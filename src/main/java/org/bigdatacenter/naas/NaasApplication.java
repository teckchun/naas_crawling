package org.bigdatacenter.naas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NaasApplication {

	public static void main(String[] args) {
		SpringApplication.run(NaasApplication.class, args);
	}

}
