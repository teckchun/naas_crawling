package org.bigdatacenter.naas.repository;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.bigdatacenter.naas.model.TechnologyTransfer;
import org.bigdatacenter.naas.model.TestResearchReport;
import org.springframework.stereotype.Repository;

@Repository
public interface TestResearchReportRepository {
	
	@Insert("<script> "+
			"INSERT INTO NAAS_DB.시험연구보고서(번호,연도,분야,제목,연구자)"
			+ "VALUES"
			+ "<foreach collection='testResearchReports' item='testResearchReport' separator=',' >" +
			" (#{testResearchReport.NO},"
			+ "#{testResearchReport.publishedYear},"
			+ "#{testResearchReport.field},"
			+ "#{testResearchReport.title},"
			+ "#{testResearchReport.researcher})"
			+ "</foreach> " 
			+ "</script> ")
    public void saveBatch(@Param("testResearchReports") List<TestResearchReport> testResearchReports);

}
