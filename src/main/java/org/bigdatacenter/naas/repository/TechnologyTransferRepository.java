package org.bigdatacenter.naas.repository;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.bigdatacenter.naas.model.TechnologyTransfer;
import org.springframework.stereotype.Repository;

@Repository
public interface TechnologyTransferRepository {
	
	@Insert("<script> "+
			"INSERT INTO NAAS_DB.기술이전(번호,연도,분야,제목,작성자)"
			+ "VALUES"
			+ "<foreach collection='technologyTransfers' item='technologyTransfer' separator=',' >" +
			" (#{technologyTransfer.NO},"
			+ "#{technologyTransfer.publishedYear},"
			+ "#{technologyTransfer.field},"
			+ "#{technologyTransfer.title},"
			+ "#{technologyTransfer.writer})"
			+ "</foreach> " 
			+ "</script> ")
    public void saveBatch(@Param("technologyTransfers") List<TechnologyTransfer> technologyTransfers);

}
