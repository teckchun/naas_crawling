package org.bigdatacenter.naas.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseModel {
	
	@JsonProperty("번호")
	private String NO;
	
	@JsonProperty("연도")
	private String publishedYear;
	
	@JsonProperty("분야")
	private String field;
	
	@JsonProperty("제목")
	private String title;

	public String getNO() {
		return NO;
	}

	public void setNO(String nO) {
		NO = nO;
	}

	public String getPublishedYear() {
		return publishedYear;
	}

	public void setPublishedYear(String publishedYear) {
		this.publishedYear = publishedYear;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	

}
