package org.bigdatacenter.naas.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Publication extends BaseModel {
	
	@JsonProperty("주저자")
	private String mainAuthor;

	@JsonProperty("구분")
	private String division;

	public String getMainAuthor() {
		return mainAuthor;
	}

	public void setMainAuthor(String mainAuthor) {
		this.mainAuthor = mainAuthor;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	
	
	
	

}
