package org.bigdatacenter.naas.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TechnologyTransfer extends BaseModel{
		
	@JsonProperty("작성자")
	private String writer;

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}
	
	
}
