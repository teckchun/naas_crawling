package org.bigdatacenter.naas.service;

public interface TechnologyTransferService {
	public boolean scrapTechnologyTransfer();
	public boolean scrapTechnologyTransferNoFilter();
}
