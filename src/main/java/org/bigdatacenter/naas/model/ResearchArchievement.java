package org.bigdatacenter.naas.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResearchArchievement extends BaseModel {
	
	
	@JsonProperty("작성자")
	private String writer;
	
	@JsonProperty("상세내역")
	private String detail;

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	

	
	
	
}
