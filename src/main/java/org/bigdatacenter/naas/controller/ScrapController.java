package org.bigdatacenter.naas.controller;

import org.bigdatacenter.naas.service.PublicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/mainController")
public class ScrapController {
	
	@Autowired
	private PublicationService publicationService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String testInsert() {
		
		if(publicationService.testInsert()) {
			return "SUCCESS";
		}else {
			return "ERROR";
		}
		
		
		
	}
}
